using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SnakeControl : MonoBehaviour
{
    public float speed = 2.0f;
    public float rotationSpeed = 100.0f;
    public GameObject bodyPrefab;
    public float bodyDistance = 0.5f;
    public List<Transform> bodyParts = new List<Transform>();
    public float maxRotationAngle = 90.0f;
    public GameObject imageTarget;

    private LevelController pointController;
    private Spawner SpawnersManager;

    private bool moveLeft = false;
    private bool moveRight = false;

    private void Start()
    {
        AgregarCuerpo();
        AgregarCuerpo();
        AgregarCuerpo();
        AgregarCuerpo();
        transform.parent = imageTarget.transform;
        pointController = GameObject.Find("LevelController")?.GetComponent<LevelController>();
        SpawnersManager = GameObject.Find("SpawnersManager")?.GetComponent<Spawner>();
    }

    void Update()
    {
        transform.Translate(Vector3.forward * speed * Time.deltaTime);

        if (moveLeft)
        {
            float rotationAmount = -1 * rotationSpeed * Time.deltaTime;
            float newRotationAngle = Mathf.Clamp(transform.rotation.eulerAngles.y + rotationAmount, transform.rotation.eulerAngles.y - maxRotationAngle, transform.rotation.eulerAngles.y + maxRotationAngle);
            transform.rotation = Quaternion.Euler(0.0f, newRotationAngle, 0.0f);
        }
        else if (moveRight)
        {
            float rotationAmount = rotationSpeed * Time.deltaTime;
            float newRotationAngle = Mathf.Clamp(transform.rotation.eulerAngles.y + rotationAmount, transform.rotation.eulerAngles.y - maxRotationAngle, transform.rotation.eulerAngles.y + maxRotationAngle);
            transform.rotation = Quaternion.Euler(0.0f, newRotationAngle, 0.0f);
        }
    }

    public void OnPointerDownLeft()
    {
        moveLeft = true;
    }

    public void OnPointerUpLeft()
    {
        moveLeft = false;
    }

    public void OnPointerDownRight()
    {
        moveRight = true;
    }

    public void OnPointerUpRight()
    {
        moveRight = false;
    }

    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag("Raton"))
        {
            SpawnersManager.RemoveObject(other.gameObject);
            pointController.IncreaseScore(1);
            AgregarCuerpo();
        }

        if (other.gameObject.CompareTag("Comida"))
        {
            Destroy(other.gameObject);
            pointController.IncreaseScore(1);
            AgregarCuerpo();
        }

        if (other.gameObject.CompareTag("Pared") || other.gameObject.CompareTag("Cuerpo"))
        {
            pointController.EndGame();
        }
    }

    void AgregarCuerpo()
    {
        if (bodyParts.Count == 0)
        {
            GameObject newBodyPart = Instantiate(bodyPrefab, transform.position - transform.forward * bodyDistance, transform.rotation);
            bodyParts.Add(newBodyPart.transform);
        }
        else
        {
            GameObject newBodyPart = Instantiate(bodyPrefab, bodyParts[bodyParts.Count - 1].position - bodyParts[bodyParts.Count - 1].forward * bodyDistance, bodyParts[bodyParts.Count - 1].rotation);
            bodyParts.Add(newBodyPart.transform);
        }

        bodyParts[bodyParts.Count - 1].parent = imageTarget.transform;

        if (bodyParts.Count > 2)
        {
            bodyParts[bodyParts.Count - 1].tag = "Cuerpo";
        }
    }

    void LateUpdate()
    {
        if (bodyParts.Count > 0)
        {
            bodyParts[0].position = Vector3.Lerp(bodyParts[0].position, transform.position - transform.forward * bodyDistance, Time.deltaTime * speed);
            for (int i = 1; i < bodyParts.Count; i++)
            {
                Vector3 offset = bodyParts[i - 1].position - bodyParts[i].position;
                float distance = offset.magnitude;
                Vector3 direction = offset.normalized;
                Vector3 targetPosition = bodyParts[i - 1].position - direction * bodyDistance;
                bodyParts[i].position = Vector3.Lerp(bodyParts[i].position, targetPosition, Time.deltaTime * speed);
                Quaternion targetRotation = Quaternion.LookRotation(direction, Vector3.up);
                float rotationAngle = Quaternion.Angle(bodyParts[i].rotation, targetRotation);
                if (rotationAngle > maxRotationAngle)
                {
                    float t = maxRotationAngle / rotationAngle;
                    bodyParts[i].rotation = Quaternion.Slerp(bodyParts[i].rotation, targetRotation, t);
                }
                else
                {
                    bodyParts[i].rotation = Quaternion.Lerp(bodyParts[i].rotation, targetRotation, Time.deltaTime * rotationSpeed);
                }
            }
        }
    }

    public void ToggleBodyParts()
    {
        foreach (Transform bodyPart in bodyParts)
        {
            bodyPart.gameObject.SetActive(!bodyPart.gameObject.activeSelf);
        }
    }

    public void StopSnake()
    {
        speed = 0f;
        rotationSpeed = 0f;
    }
}