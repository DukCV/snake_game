using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class LevelController : MonoBehaviour
{
    public TextMeshPro countdownText;
    public TextMeshPro scoreText;
    public int scoreGoal = 100;

    public int UnlockLevel = 0;
    public float countdownTime = 60.0f;
    public GameObject victoryScreen;
    public GameObject gameOverScreen;

    private float currentTime = 0.0f;
    private int currentScore = 0;
    public bool gameEnded = false;

    private bool isPaused = false;

    private SnakeControl snakeControl;
    private LevelsManager levelsManager;

    // Start is called before the first frame update
    void Start()
    {
        currentTime = countdownTime;
        UpdateCountdownText();
        UpdateScoreText();
    }

    public void PauseResumeGame()
    {
        isPaused = !isPaused;
    }

    // Update is called once per frame
    void Update()
    {
        if (!gameEnded && !isPaused)
        {
            currentTime -= Time.deltaTime;
            UpdateCountdownText();

            if (currentTime <= 0.0f)
            {
                EndGame();
                countdownText.text = "Tiempo: 00:00:00";
            }
        }
    }

    public void IncreaseScore(int amount)
    {
        if (!gameEnded)
        {
            currentScore += amount;
            UpdateScoreText();

            if (currentScore >= scoreGoal)
            {
                Victory();
            }
        }
    }

    void UpdateCountdownText()
    {
        int minutes = Mathf.FloorToInt(currentTime / 60.0f);
        int seconds = Mathf.FloorToInt(currentTime % 60.0f);
        int milliseconds = Mathf.FloorToInt((currentTime % 1.0f) * 100.0f);
        countdownText.text = "Tiempo: " + string.Format("{0:00}:{1:00}:{2:00}", minutes, seconds, milliseconds);
    }

    void UpdateScoreText()
    {
        scoreText.text = "Puntaje: " + currentScore.ToString() + "/" + scoreGoal.ToString();
    }

    void Victory()
    {
        snakeControl = GameObject.Find("Serpiente")?.GetComponent<SnakeControl>();
        levelsManager = GameObject.Find("Level Manager")?.GetComponent<LevelsManager>();
        gameEnded = true;
        victoryScreen.SetActive(true);
        snakeControl.StopSnake();
        levelsManager.UnlockLevel(UnlockLevel);
    }

    public void EndGame()
    {
        snakeControl = GameObject.Find("Serpiente")?.GetComponent<SnakeControl>();
        gameEnded = true;
        gameOverScreen.SetActive(true);
        snakeControl.StopSnake();
    }
}
