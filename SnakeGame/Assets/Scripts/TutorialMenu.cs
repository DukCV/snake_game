using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialMenu : MonoBehaviour
{
    public GameObject[] objects;
    private int currentObjectIndex = 0;

    public void NextObject()
    {
        objects[currentObjectIndex].SetActive(false);
        currentObjectIndex++;
        if (currentObjectIndex >= objects.Length)
        {
            currentObjectIndex = 0;
        }
        objects[currentObjectIndex].SetActive(true);
    }

    public void PreviousObject()
    {
        objects[currentObjectIndex].SetActive(false);
        currentObjectIndex--;
        if (currentObjectIndex < 0)
        {
            currentObjectIndex = objects.Length - 1;
        }
        objects[currentObjectIndex].SetActive(true);
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}
