using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseAI : MonoBehaviour
{
    public float speed = 5.0f;
    public LayerMask obstacleLayer;

    private Vector3 movement;
    private float timer = 0.0f;
    private float changeDirectionTime = 2.0f;
    private float detectionRadius = 1.0f;
    private float avoidObstacleDistance = 2.0f;

    private LevelController pointController;

    private void Start()
    {
        pointController = GameObject.Find("LevelController").GetComponent<LevelController>();
    }

    void Update()
    {
        if (!pointController.gameEnded)
        {
            RaycastHit hit;
            if (Physics.SphereCast(transform.position, detectionRadius, transform.forward, out hit, avoidObstacleDistance, obstacleLayer))
            {
                transform.rotation = Quaternion.LookRotation(transform.position - hit.point, Vector3.up);
            }
            else
            {
                movement = transform.forward * speed * Time.deltaTime;
                transform.position += movement;

                timer += Time.deltaTime;
                if (timer > changeDirectionTime)
                {
                    timer = 0.0f;
                    float randomRotation = Random.Range(0.0f, 360.0f);
                    transform.rotation = Quaternion.Euler(0.0f, randomRotation, 0.0f);
                }
            }
        }
    }
}

