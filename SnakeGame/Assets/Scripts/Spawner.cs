using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public GameObject prefabToSpawn;
    public float spawnInterval = 3.0f;
    public int maxSpawnedObjects = 5;
    public int minSpawnedObjects = 2;
    public GameObject objectsParent;
    public bool spawnEnabled = true;

    public List<GameObject> spawnedObjects = new List<GameObject>();
    private List<GameObject> spawners = new List<GameObject>();

    void Start()
    {
        GameObject[] spawnersArray = GameObject.FindGameObjectsWithTag("Spawner");
        foreach (GameObject spawner in spawnersArray)
        {
            spawners.Add(spawner);
        }
        StartCoroutine(SpawnObjects());
    }

    IEnumerator SpawnObjects()
    {
        while (true)
        {
            if (spawnEnabled && spawnedObjects.Count < maxSpawnedObjects)
            {
                int randomIndex = Random.Range(0, spawners.Count);
                GameObject spawner = spawners[randomIndex];

                GameObject spawnedObject = Instantiate(prefabToSpawn, spawner.transform.position, Quaternion.identity);
                spawnedObject.transform.parent = objectsParent.transform;

                spawnedObjects.Add(spawnedObject);

                if (spawnedObjects.Count >= maxSpawnedObjects)
                {
                    spawnEnabled = false;
                }
            }

            yield return new WaitForSeconds(spawnInterval);
        }
    }

    public void RemoveObject(GameObject objectToRemove)
    {
        spawnedObjects.Remove(objectToRemove);
        Destroy(objectToRemove);

        if (spawnedObjects.Count < minSpawnedObjects)
        {
            spawnEnabled = true;
        }
    }


    public void ToggleSpawner()
    {
        spawnEnabled = !spawnEnabled;
    }

    public void TogglePrefabs()
    {
        foreach (GameObject obj in spawnedObjects)
        {
            obj.SetActive(!obj.activeSelf);
        }
    }
}
