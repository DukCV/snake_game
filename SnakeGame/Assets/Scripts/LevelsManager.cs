using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LevelsManager : MonoBehaviour
{
    public List<Button> levelButtons;
    public int levelsUnlocked = 1;

    void Start()
    {
        levelsUnlocked = PlayerPrefs.GetInt("levelsUnlocked", 1);

        for (int i = 0; i < levelButtons.Count; i++)
        {
            int levelIndex = i + 1;
            levelButtons[i].onClick.AddListener(() => { LoadLevel(levelIndex); });

            if (levelIndex > levelsUnlocked)
            {
                levelButtons[i].interactable = false;
            }
        }
    }

    public void LoadLevel(int levelIndex)
    {
        SceneManager.LoadScene(levelIndex);
    }

    public void UnlockLevel(int levelIndex)
    {
        if (levelIndex == levelsUnlocked + 1)
        {
            levelsUnlocked = levelIndex;
            PlayerPrefs.SetInt("levelsUnlocked", levelsUnlocked);

            if (levelButtons != null && levelIndex <= levelButtons.Count)
            {
                levelButtons[levelIndex - 1].interactable = true;
            }
        }
    }

    public void Reset()
    {
        PlayerPrefs.DeleteAll();
        levelsUnlocked = 1;

        for (int i = 0; i < levelButtons.Count; i++)
        {
            if (i >= levelsUnlocked)
            {
                levelButtons[i].interactable = false;
            }
        }
    }

}


