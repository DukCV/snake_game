using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PauseMenu : MonoBehaviour
{
    public GameObject pausePanel;

    private bool isPaused = false;

    private LevelController pointController;

    private void Start()
    {
        pointController = GameObject.Find("LevelController").GetComponent<LevelController>();
    }

    void Update()
    {
        if (PlayerPrefs.GetInt("IsFirstTime", 1) != 1)
        {
            if (Input.GetKeyDown(KeyCode.Escape) && !pointController.gameEnded)
            {
                if (isPaused)
                {
                    ResumeGame();
                }
                else
                {
                    PauseGame();
                }
            }
        }
    }

    public void PauseGame()
    {
        Time.timeScale = 0f;
        isPaused = true;
        pausePanel.SetActive(true);
    }

    public void ResumeGame()
    {
        Time.timeScale = 1f;
        isPaused = false;
        pausePanel.SetActive(false);
    }

    public void ReturnToMenu()
    {
        Time.timeScale = 1f;
        SceneManager.LoadScene(0);
    }

    void OnApplicationPause(bool pauseStatus)
    {
        if (PlayerPrefs.GetInt("IsFirstTime", 1) != 1)
        {
            if (pauseStatus && isPaused == false && !pointController.gameEnded)
            {
                PauseGame();
            }
        }
    }

}
