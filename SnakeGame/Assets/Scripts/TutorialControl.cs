using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TutorialControl : MonoBehaviour
{
    public GameObject tutorialObject;
    public GameObject[] tutorialPanels;
    private int currentPanelIndex = 0;

    void Start()
    {
        if (PlayerPrefs.GetInt("IsFirstTime", 1) == 1)
        {
            tutorialObject.SetActive(true);
            Time.timeScale = 0;
            ShowPanel(currentPanelIndex);
            PlayerPrefs.SetInt("IsFirstTime", 0);
        }
        else
        {
            tutorialObject.SetActive(false);
            Time.timeScale = 1;
        }
    }

    public void ShowNextPanel()
    {
        currentPanelIndex++;
        if (currentPanelIndex >= tutorialPanels.Length)
        {
            CloseTutorial();
        }
        else
        {
            ShowPanel(currentPanelIndex);
        }
    }

    public void ShowPreviousPanel()
    {
        currentPanelIndex--;
        if (currentPanelIndex < 0)
        {
            currentPanelIndex = 0;
        }
        ShowPanel(currentPanelIndex);
    }

    private void ShowPanel(int index)
    {
        for (int i = 0; i < tutorialPanels.Length; i++)
        {
            tutorialPanels[i].SetActive(i == index);
        }
    }

    public void CloseTutorial()
    {
        tutorialObject.SetActive(false);
        Time.timeScale = 1;
    }
}
