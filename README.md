# SnakeGame

Este repositorio contiene el código fuente y los recursos necesarios para construir un juego móvil de la víbora en 3D en realidad aumentada utilizando Unity 2021.3.14f1 y Vuforia, una plataforma de desarrollo de realidad aumentada.

El objetivo del juego es simple: controlar a la víbora y guiarla hacia la comida antes de que se termine el tiempo, evitando chocar contra las paredes, obstaculos o contra su propio cuerpo. El juego se juega en un entorno 3D que se superpone a la cámara del dispositivo móvil del jugador a través de la tecnología de realidad aumentada.

El juego utiliza los Image Target de Vuforia como marcadores para colocar la escena 3D en el mundo real. Cuando el jugador enfoca la cámara del dispositivo en uno de estos marcadores, la escena se superpone sobre el mundo real. Los marcadores se pueden imprimir y colocar en cualquier superficie plana, como una mesa o un tablero, para jugar en un entorno físico.

# El repositorio incluye:

Código fuente de Unity para el juego de la víbora en 3D en realidad aumentada.

Modelos 3D para la víbora y la comida.

Texturas y materiales para los modelos.

Imágenes de los Image Target de Vuforia para imprimir y utilizar como marcadores.

APK para instalar la aplicacion

El juego ha sido probado en dispositivos Android de la version 6 en adelante.

# Cómo jugar
Para jugar al juego de la víbora en 3D en realidad aumentada, sigue estos pasos:

Descarga e instala la aplicación en tu dispositivo móvil.

Imprime y coloca los Image Target de Vuforia en una superficie plana.

Abre la aplicación y enfoca la cámara del dispositivo en uno de los marcadores.

La escena del juego se superpondrá en el mundo real.

Controla a la víbora utilizando los controles en pantalla y guíala hacia la comida.

Evita chocar contra las paredes, obstaculos o contra tu propio cuerpo.

Gana puntos comiendo antes de que se termine el tiempo.

# ¡Diviértete!
